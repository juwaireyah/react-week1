import React, {Component} from 'react';
import {Card, CardImg, CardText, CardBody, CardTitle,Media, Breadcrumb, BreadcrumbItem,
    Button, Modal, ModalHeader, ModalBody, Label, Col, Row} from 'reactstrap';
import {Link} from 'react-router-dom';
import {Control, LocalForm, Errors} from 'react-redux-form';
import {Loading} from './LoadingComponent';
import {baseUrl} from '../shared/baseUrl';
import {FadeTransform, Fade, Stagger} from 'react-animation-components';
   
    /* componentDidMount(){
        console.log("Dishdetail Component componentDidMount invoked");
    }

    componentDidUpdate(){
        console.log("Dishdetail Component componentDidUpdate invoked");
    }*/

    function RenderDish({dish}){
        console.log(dish);
        if (dish !== null){
            return(
                <FadeTransform in 
                    transformProps={{
                        exitTransform:'scale(0.5) translateY(-50%)'
                    }}>
                    <Card>
                        <CardImg width="100%" src={baseUrl+dish.image} alt={dish.name} />
                        <CardBody>
                            <CardTitle>{dish.name}</CardTitle>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                    </Card>
                </FadeTransform>
            );
        }
        else{
            return(<div></div>);
        }
    }
    function RenderComments({comnt,postComment, dishId}){
        if (comnt !== null){
            return (
                
                    <Fade in>
                        <div key={comnt.id} className="col-12 mt-5">
                            <Media tag="li">
                                <Media body >
                                    <p>{comnt.comment}</p>
                                    <p>-- {comnt.author}, {new Intl.DateTimeFormat('en-US', {year:'numeric', month:'short', day:'2-digit'}).format(new Date(Date.parse(comnt.date)))}</p>
                                </Media>
                            </Media>
                        </div>
                    </Fade>
                
            );
           
        }
        else{
            return(<div></div>);
        }
        
    }
    const DishDetail = (props) => {
        console.log(props);
        if (props.isLoading){
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess){
            return(
                <div className="container">
                    <div className="row">
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else if (props.dish){
            //const filterComnt = props.comments.filter((comment)=>comment.dishId === parseInt(0) );
            const comment = props.comments.map( (comment) => {
                return (
                    <Stagger in >
                        <RenderComments postComment={props.postComment} dishId={props.dish.id} key={comment.id} comnt={comment} />  
                    </Stagger>
                );
            });

            return (
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to='/menu'>Menu</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem active>{props.dish.name}
                                {/* <Link to='/menu'>Menu</Link> */}
                            </BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.dish.name}</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-5 col-12 m-1">
                            <RenderDish dish={props.dish} />
                        </div>
                        <div className="col-md-5 col-12 m-1">
                            <h4>Comments</h4>
                                <Media list> 
                                    {comment} 
                                </Media>
                            
                            <CommentForm dishId={props.dish.id} postComment={props.postComment}></CommentForm>
                        </div>
                    </div>
                </div>
            );
                
            
        }
        else{
            return(<div></div>);
        }
    }

    const required = (val) => val && val.length;
    const maxLength = (len) => (val) => !(val) || (val.length <= len);
    const minLength = (len) => (val) => val && (val.length >= len);

    class CommentForm extends Component{
        constructor(props){
            super(props);
            this.state = {
                isNavOpen: false,
                isModalOpen: false
            };
    
            this.toggleModal = this.toggleModal.bind(this);
            this.handleSubmit = this.handleSubmit.bind(this);
        }
    
        toggleModal(){
            this.setState({
                isModalOpen: !this.state.isModalOpen
            });
        }
    
        handleSubmit(values){
            this.toggleModal();
            this.props.postComment(this.props.dishId, values.rating, values.author, values.comment );
           // alert("Current state is: "+JSON.stringify(values));
        }
        
        render(){
            return(
                <>
                <div>
                   <Button onClick={this.toggleModal} outline><span className="fa fa-pencil fa-lg"></span>Submit Comment</Button>
                   <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                        <ModalBody>
                            <LocalForm onSubmit={(values)=>this.handleSubmit(values)}>
                                 <Row className="form-group">
                                    <Label htmlFor="rating" md={12}>Rating</Label>
                                    <Col md={12}>
                                        <Control.select model=".rating" name="rating" className="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </Control.select>
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Label htmlFor="author" md={12}>Your Name</Label>
                                    <Col md={12}>
                                        <Control.text model=".author" id="author" name="author"
                                            placeholder="Your Name"
                                            className="form-control"
                                            validators={{
                                                required, minLength: minLength(3), maxLength: maxLength(15)
                                            }}
                                        />
                                        <Errors
                                            className="text-danger"
                                            model=".author"
                                            show="touched"
                                            messages={{
                                                required: 'Required, ',
                                                minLength: 'Must be greater than 2 characters',
                                                maxLength: 'Must be 15 characters or less'
                                            }}
                                            />
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Label htmlFor="comment" md={12}>Comment</Label>
                                    <Col md={12}>
                                        <Control.textarea model=".comment" id="comment" name="comment" rows="6" className="form-control"/>
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Col md={{size:10}}>
                                        <Button type="submit" color="primary">Submit</Button>
                                    </Col>
                                </Row>
                            </LocalForm>
                        </ModalBody>
                    </Modal>
                </div>
                </>
            );
        }
    }



export default DishDetail;